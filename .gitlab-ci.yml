################################
# GitLab CI/CD
# gitlab.com/spaceleiden/portal
#

image: docker:20

variables:
  DOCKER_DRIVER: overlay2
  IMAGE_NAME: registry.gitlab.com/spaceleiden/portal
  DOCKER_BUILDKIT: 1
  MYSQL_DATABASE: spaceleiden
  MYSQL_USER: spaceleiden
  MYSQL_ROOT_PASSWORD: this-is-not-a-real-password
  MYSQL_PASSWORD: "this-is-not-a-real-password"

default:
  interruptible: true

stages:
  - prepare
  - build
  - test
  - tag
  - deploy


########################
# Shorthands and caches
#

.npm-cache: &npm-cache
  key:
    files:
      - package-lock.json
    prefix: npm
  paths:
    - node_modules/
  policy: pull

.composer-cache: &composer-cache
  key:
    files:
      - package.lock
    prefix: composer
  paths:
    - vendor/
  policy: pull

.sast-analyzer: &sast-analyzer
  stage: prepare
  image: "$SAST_ANALYZER_IMAGE"
  allow_failure: true
  artifacts:
    reports:
      sast: gl-sast-report.json
  variables:
    SAST_ANALYZER_IMAGE_TAG: 2
    SAST_EXCLUDED_PATHS: spec, test, tests, tmp, vendor
    SECURE_ANALYZERS_PREFIX: "registry.gitlab.com/gitlab-org/security-products/analyzers"
    SEARCH_MAX_DEPTH: 4
  script:
    - /analyzer run


######################
# Prepare
#

prepare_backend:
  stage: prepare
  image: composer:2
  cache:
    <<: *composer-cache
    policy: pull-push
  before_script:
    - cp -f .env.ci .env
  script:
    - composer install -q --prefer-dist --no-ansi --no-interaction --no-progress

prepare_frontend:
  stage: prepare
  image: node:16-alpine
  cache:
    <<: *npm-cache
    policy: pull-push
  script:
    - npm ci


######################
# Analyze 
#

analyze_backend:
  stage: prepare
  image: composer:2
  cache: *composer-cache
  before_script:
    - curl -sS https://get.symfony.com/cli/installer | bash
  script:
    - composer lint:check -- --no-progress-bar --no-error-table
    - /root/.symfony/bin/symfony security:check

analyze_frontend:
  stage: prepare
  image: node:16-alpine
  cache:
    <<: *npm-cache
    policy: pull-push
  script:
    - npm run lint
    - npm audit

# Experimental: SAST implementation to test code security.
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml
sast_frontend:
  extends: .sast-analyzer
  variables:
    SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/eslint:$SAST_ANALYZER_IMAGE_TAG"
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.html'
        - '**/*.js'

sast_backend:
  extends: .sast-analyzer
  variables:
    SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/phpcs-security-audit:$SAST_ANALYZER_IMAGE_TAG"
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.php'

#####################
# Build
#

build_image:
  stage: build
  needs:
    - prepare_frontend
    - prepare_backend
    - analyze_backend
    - analyze_frontend
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
    - docker pull $IMAGE_NAME:latest || true # or pull & cache from $CI_COMMIT_REF_SLUG
  script:
    - docker build --cache-from $IMAGE_NAME:latest -t $IMAGE_NAME:$CI_COMMIT_SHORT_SHA .
    - docker image push $IMAGE_NAME:$CI_COMMIT_SHORT_SHA
  after_script:
    - docker logout $CI_REGISTRY
  tags:
    - builder


####################
# Docker tag images
#

.docker-tag: &docker-tag
  stage: tag
  variables:
    GIT_STRATEGY: none
  needs:
    - build_image
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
  script:
    - docker tag $IMAGE_NAME:$CI_COMMIT_SHORT_SHA $IMAGE_NAME:$TAG_NAME
    - docker push $IMAGE_NAME:$TAG_NAME
  after_script:
    - docker logout $CI_REGISTRY
  tags:
    - builder

tag_main:
  extends: .docker-tag
  variables:
    TAG_NAME: latest
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

tag_develop:
  extends: .docker-tag
  variables:
    TAG_NAME: develop
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop"'

# TODO: We could also tag $CI_COMMIT_REF_SLUG for MR branches.


###################
# Test
#

test_backend:
  stage: test
  image:
    name: $IMAGE_NAME:$CI_COMMIT_SHORT_SHA
    entrypoint: ['/usr/local/bin/docker-php-entrypoint']
  services:
    - mysql:8
  cache: *composer-cache
  needs:
    - build_image
  before_script:
    - cp .env.ci .env
    - php artisan migrate:fresh --seed
  script:
    - vendor/bin/phpunit --coverage-text --colors=never --log-junit report.xml
  artifacts:
    reports:
      junit: report.xml

# TODO: Implement test_frontend


####################
# Deployment stages
#

deploy_staging:
  stage: deploy
  needs:
    - build_image
    - tag_develop
    - test_backend
  tags:
    - staging
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop"'
  script:
    - docker stack deploy --prune -c docker-compose.staging.yml portal_staging

deploy_production:
  stage: deploy
  needs:
    - build_image
    - tag_main
    - test_backend
  tags:
    - production
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
  rules:
    - when: manual
      if: '$CI_COMMIT_BRANCH == "main"'
  script:
    - docker stack deploy --prune -c docker-compose.production.yml portal_production
