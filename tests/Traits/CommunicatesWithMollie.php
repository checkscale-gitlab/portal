<?php

declare(strict_types=1);

namespace Tests\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Mollie\Api\MollieApiClient;

trait CommunicatesWithMollie
{
    protected array $mollieRequestHistory = [];

    /**
     * Make Mollie's Guzzle instance use a mock handler.
     *
     * @see http://docs.guzzlephp.org/en/stable/testing.html
     *
     * @throws \Mollie\Api\Exceptions\IncompatiblePlatform
     * @throws \Mollie\Api\Exceptions\UnrecognizedClientException
     */
    public function mockMollie(): MockHandler
    {
        $handler = HandlerStack::create(
            $mockHandler = new MockHandler()
        );

        $handler->push(
            Middleware::history($this->mollieRequestHistory)
        );

        $guzzle = new Client([
            'handler' => $handler,
        ]);

        $this->app->singleton('mollie.api.client', function () use ($guzzle) {
            return new MollieApiClient($guzzle);
        });

        return $mockHandler;
    }
}
