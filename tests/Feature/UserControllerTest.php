<?php

declare(strict_types=1);

namespace Tests\Feature;

use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\CommunicatesWithMollie;

class UserControllerTest extends TestCase
{
    use CommunicatesWithMollie;

    use DatabaseTransactions;

    /**
     * Test that the user can sign up for the application
     */
    public function testSignupWorks(): void
    {
        $responseStack = $this->mockMollie();
        $responseStack->append(
            new Response(200, [], json_encode([
                'id' => 'test_id',
            ])),
            new Response(200, [], json_encode([
                '_links' => [
                    'checkout' => [
                        'href' => 'minkukelsunited.be',

                    ],
                ],
            ])),
        );

        $response = $this->postJson(route('signup'), [
            'first_name' => 'John',
            'last_name' => 'Appleseed',
            'email' => 'john.appleseed@example.com',
            'password' => 'hunter2hunter2hunter2',
            'phone_number' => '0612345678',
            'postal_code' => '1234',
            'birthdate' => '01-01-2000',
            'opt_in_news' => true,
            'agree_terms' => true,
        ]);

        $response
            ->dump()
            ->assertStatus(200);

        $this->assertSame('minkukelsunited.be', $response->json('data.href'));
    }
}
