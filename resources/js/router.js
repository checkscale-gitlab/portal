import VueRouter from 'vue-router'

import Home from './vue/views/Home'
import User from './vue/views/User'
import Materials from './vue/views/Materials'
import AdminNieuws from './vue/views/admin/Nieuws'
import AdminDeelnemers from './vue/views/admin/Deelnemers'
import AdminActiviteiten from './vue/views/admin/Activiteiten'
import AdminSettings from './vue/views/admin/Settings'
import Login from './vue/views/Login'
import Signup from './vue/views/Signup'
import SignupCallback from './vue/views/SignupCallback'
import ResetPassword from './vue/views/ResetPassword'
import Donate from './vue/views/Donate'
import DonateCallback from './vue/views/DonateCallback'
import Archive from './vue/views/Archive'

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/signup',
      name: 'Signup',
      meta: { title: false },
      component: Signup
    },
    {
      path: '/signup/callback',
      name: 'SignupCallback',
      meta: { title: false },
      component: SignupCallback
    },
    {
      path: '/login',
      name: 'Login',
      meta: { title: false },
      component: Login
    },
    {
      path: '/reset-password',
      name: 'ResetPassword',
      meta: { title: 'Wachtwoord herstellen' },
      component: ResetPassword
    },
    {
      path: '/home',
      name: 'Home',
      meta: { title: 'The Space Leiden Portal' },
      component: Home
    },
    {
      path: '/user',
      name: 'User',
      meta: { title: 'Profiel' },
      component: User
    },
    {
      path: '/donate',
      name: 'Doneren',
      meta: { title: 'Doneren aan The Space' },
      component: Donate
    },
    {
      path: '/donate/callback',
      name: 'Doneren Callback',
      meta: { title: false },
      component: DonateCallback
    },
    {
      path: '/archive',
      name: 'Archief',
      meta: { title: 'Archief' },
      component: Archive
    },
    {
      path: '/materials',
      name: 'Materials',
      meta: { title: 'Materialen' },
      component: Materials
    },
    {
      path: '/admin/nieuws',
      name: 'Nieuws',
      meta: { title: 'Nieuws' },
      component: AdminNieuws
    },
    {
      path: '/admin/deelnemers',
      name: 'Deelnemers',
      meta: { title: 'Deelnemers' },
      component: AdminDeelnemers
    },
    {
      path: '/admin/activiteiten',
      name: 'Activiteiten',
      meta: { title: 'Activiteiten' },
      component: AdminActiviteiten
    },
    {
      path: '/admin/settings',
      name: 'Instellingen',
      meta: { title: 'Instellingen' },
      component: AdminSettings
    }
  ]
})
