import Vue from 'vue'
import App from './vue/App'
import axios from './plugins/axios'

import VueRouter from 'vue-router'
import router from './router'

import vuetify from './plugins/vuetify'

require('./bootstrap')

Vue.use(VueRouter)
Vue.use(axios)

new Vue({
  render: h => h(App),
  router,
  vuetify
}).$mount('#app')
