'use strict'

import axios from 'axios'
import { EventBus } from './events-bus'

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const config = {
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
}

const _axios = axios.create(config)

_axios.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
_axios.interceptors.response.use(
  function (response) {
    // If the API returns an error, we return the associated message
    if (response.data.error !== false) {
      return Promise.reject(response.data)
    }

    // Else we return the data
    return response.data
  },
  function (error) {
    // This will fire when the http status code isn't in the 200 range
    // if the error is 401 specifically we fire the unauthorized event
    // on the eventbus.
    if (error.response.status === 401) {
      EventBus.$emit('unauthorized')
    }

    // Now here is stront aan de knikker, there is a backend error that
    // we don't know how to deal with, the solution is echoing the
    // accompanying message with a snackbar.
    return Promise.reject(error.response.data)
  }
)

Plugin.install = function (Vue) {
  Vue.$axios = _axios
  Object.defineProperties(Vue.prototype, {
    axios: {
      get () {
        return _axios
      }
    },
    $axios: {
      get () {
        return _axios
      }
    }
  })
}

export default Plugin
