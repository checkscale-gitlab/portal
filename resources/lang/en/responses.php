<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API call responses.
    |--------------------------------------------------------------------------
    |
    */

    'success' => 'The request was successful.',
    'welcome' => 'Welcome back!',
    'failed' => 'The request was not successful.',
    'not_implemented' => 'This request is not implemented yet.',

    'finance' => [
        'mandate' => [
            'ready' => 'Mandate payment has been created and is ready.',
        ],
        'subscription' => [
            'cancel' => [
                'unable' => 'Nothing to cancel, user has no valid subscription.',
                'success' => 'Subscription is cancelled.',
            ],
            'reactivate' => [
                'no_mandate' => 'User has no mandate, unable to reactivate.',
                'already_has_subscription' => 'Active subscription already exists.',
            ],
            'create' => [
                'no_mandate' => 'No valid mandate exists (yet).',
                'already_active' => 'Subscription is already activated.',
                'already_exists' => 'Subscription already exists and is not active or canceled.',
                'success' => 'Subscription has been activated.',
            ]
        ],
    ],

];
