<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Je wachtwoord is opnieuw ingesteld!',
    'throttled' => 'Wacht even voordat je dat opnieuw probeert.',
    'token' => 'De wachtwoord herstel code is niet geldig.',

    // Identical messages to prevent user enumeration.
    'sent' => "Als het e-mailadres bij ons bekend is, zal je snel een e-mail ontvangen.",
    'user' => "Als het e-mailadres bij ons bekend is, zal je snel een e-mail ontvangen.",

];
