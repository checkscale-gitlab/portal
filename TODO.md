# TODO
backend api:
- [ ] **Jeanne** beheer deelnemersadministratie dashboard

[comment]: <> (  https://templates-flatlogic.herokuapp.com/vue-material/#/user/profile)
   - [ ] Google agenda
   - [ ] Eigen gegevens
   - [ ] Linkjes naar:
      - Opgenomen talks
      - Reserveringssysteem
      - Edit eigen gegevens pagina
   - [ ] tabelletjes grafiekjes (Jeanne)
- [ ] **Niels** authenticatie met users (rollen: deelnemer,admin) (spatie/roles-permissions-ofzo) sso keycloak
- [ ] **Sander/Niels** intern betalingssysteem via mollie-php
- [ ] **Niels** deelnemers homepage/dashboard/portal
  - [ ] **Jeffrey** archief talks, sheets en opnames
  - ~~reserveringssysteem?~~
  - ~~covid aanmelden voor bijeenkomst~~
  - ~~materiaalbeheer~~
  - [ ] **Niels** signup flow voor nieuwe deelnemers

portal.spaceleiden.nl/login
                     /signup
                     /home
                     /admin

etc
