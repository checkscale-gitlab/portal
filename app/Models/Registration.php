<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registration extends Model
{
    use HasFactory;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'activity_id',
        'purpose',
    ];

    /**
     * Get the activity for the registration
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * Get the attending user for the activity.
     */
    public function attendee()
    {
        return $this->hasOne(User::class)->whereNull('registrations.deleted_at');
    }
}
