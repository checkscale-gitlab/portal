<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'settings';

    protected $primaryKey = 'name';

    protected $fillable = [
        'name',
        'value',
        'type',
    ];

    public function getValueAttribute($value): bool|int|string
    {
        return match ($this->type) {
            'integer' => (int) $value,
            'boolean' => (bool) $value,
            default => (string) $value,
        };
    }
}
