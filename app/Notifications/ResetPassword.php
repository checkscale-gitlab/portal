<?php

declare(strict_types=1);

namespace App\Notifications;

//use App\Mail\ResetPassword as ResetPasswordMail;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordIlluminate;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class ResetPassword extends ResetPasswordIlluminate
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a new notification instance.
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    protected function buildMailMessage($url)
    {
        /* return (new ResetPasswordMail($url)); */

        return (new MailMessage())
            ->subject(Lang::get('Herstel je wachtwoord'))
            ->line(Lang::get('Je ontvangt deze e-mail omdat we voor jouw account een wachtwoord herstel aanvraag hebben gekregen.'))
            ->action(Lang::get('Nieuw wachtwoord instellen'), $url)
            ->line(Lang::get('Deze link is :count minuten geldig.', [
                'count' => config('auth.passwords.' . config('auth.defaults.passwords') . '.expire'),
            ]))
            ->line(Lang::get('Als je deze aanvraag niet herkent, kan je deze e-mail veilig negeren.'));
    }
}
