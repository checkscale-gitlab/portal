<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class JsonResponseServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     */
    public function boot()
    {
        $this->registerSuccessMacro();
        $this->registerErrorMacro();
        $this->registerHttpStatusMacro();
    }

    private function registerSuccessMacro()
    {
        Response::macro('success', function (string $message, array $data = []) {
            return response()->json([
                'error' => false,
                'message' => $message,
                'data' => $data,
            ]);
        });
    }

    private function registerErrorMacro()
    {
        Response::macro('error', function (string $message, array $data = []) {
            return response()
                ->json([
                    'error' => true,
                    'message' => $message,
                    'data' => $data,
                ])
                ->setStatusCode(SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR);
        });
    }

    private function registerHttpStatusMacro()
    {
        Response::macro('statusError', function (int $status, string $message, array $data = []) {
            return response()->json([
                'error' => true,
                'message' => $message,
                'data' => $data,
            ])->setStatusCode($status);
        });
    }
}
