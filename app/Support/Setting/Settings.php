<?php

declare(strict_types=1);

namespace App\Support\Setting;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

class Settings implements Arrayable
{
    private Collection $settings;

    public function __construct(Collection $settings)
    {
        $this->settings = $settings->keyBy('name');
    }

    public function get(string $name): bool|int|string
    {
        return $this->settings->get($name)->value;
    }

    public function updated_at(string $name)
    {
        return $this->settings->get($name)->updated_at;
    }

    public function toJson(): array
    {
        $j = [];
        foreach ($this->settings as $setting) {
            $j[$setting->name] = $setting->value;
        }
        return $j;
    }

    public function toArray(): array
    {
        return $this->settings->all();
    }
}
