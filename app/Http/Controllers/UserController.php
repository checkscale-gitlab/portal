<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\SignupRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Mollie\Laravel\Facades\Mollie;

class UserController extends Controller
{
    /**
     * Get and return the details of the currently authenticated user.
     */
    public function me(Request $request)
    {
        $user = $request->user();
        $userArray = $user->toArray();

        return response()->success(__('responses.welcome'), $userArray);
    }

    /**
     * Return user info plus financials from mollie.
     */
    public function finance(Request $request)
    {
        $user = $request->user();
        $userArray = $user->toArray();
        if (! $user->mollie_customer_id) {
            $userArray['subscription'] = false;
            $userArray['mandate'] = false;
        } else {
            $userArray['subscription'] = $user->subscription;
            $userArray['mandate'] = $user->mandate;
        }
        return response()->success(__('responses.success'), $userArray);
    }

    public function payments(Request $request)
    {
        $user = $request->user();
        if ($user->mollie_customer_id) {
            $payments = $user->payments;
            return response()->success(__('responses.success'), $payments);
        }
        return response()->statusError(422, __('responses.failed'), []);
    }

    /**
     * Signup a new user!
     */
    public function signup(SignupRequest $request)
    {
        try {
            $user = User::create($request->validated());
        } catch (QueryException $exception) {
            report($exception);
            return response()->error(__('responses.failed'), [
                'error_message' => $exception->getMessage(),
            ]);
        }

        // 2. create a mollie customer
        $customer = Mollie::api()->customers()->create([
            'name' => $user->full_name,
            'email' => $user->email,
            'metadata' => [
                'user_id' => $user->id,
            ],
        ]);
        $user->mollie_customer_id = $customer->id;
        $user->save();

        // TODO: Create a job to remind the user to finish the mandate payment
        // with a delay of 10 mins.

        // 3. Create and redirect a new mandate payment.
        return $this->createMandatePayment($request, $customer->id, $user);
    }

    /**
     * If we have an existing user and we need to re-create a new mandate payment
     * because for example the user has no valid/active mandates.
     */
    public function createNewMandatePayment(Request $request)
    {
        $user = $request->user();
        $customer_id = $user->mollie_customer_id;
        return $this->createMandatePayment($request, $customer_id, $user);
    }

    /**
     * Update an existing resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request)
    {
        $user = $request->user();

        // check if the given email address is in use by another member
        $emailUser = User::where('email', $request['email'])->first();
        if ($emailUser && $emailUser->id !== $user->id) {
            return response()->statusError(400, __('validation.invalid'));
        }
        $phoneUser = User::where('phone_number', $request['phone_number'])->first();
        if ($phoneUser && $phoneUser->id !== $user->id) {
            return response()->statusError(400, __('validation.invalid'));
        }

        $validated = $request->validated();

        $user->fill($validated);
        $user->save();

        return response()->success(__('responses.success'), [
            'user' => $user,
        ]);
    }

    private function createMandatePayment($request, $customer_id, $user)
    {
        $baseUrl = $request->getSchemeAndHttpHost();
        $redirectUrl = $baseUrl . '/signup/callback?customerId=' . $customer_id . '&userId=' . $user->id;
        $default_subscription = Subscription::getDefaultSubscription();
        $mandate_cost = number_format($default_subscription->amount / 2, 2);

        $payment = Mollie::api()->payments()->create([
            'amount' => [
                'currency' => $default_subscription->currency,
                'value' => $mandate_cost,
            ],
            'customerId' => $customer_id,
            'sequenceType' => 'first',
            'description' => $default_subscription->mandate_title,
            'redirectUrl' => $redirectUrl,
        ]);

        return response()->success(__('responses.finance.mandate.ready'), [
            'href' => $payment->_links->checkout->href,
        ]);
    }
}
