<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Support\Setting\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{
    public function __construct(Settings $settings)
    {
        parent::__construct($settings);
    }

    public function index(Request $request)
    {
        return response()->success(__('responses.success'), $this->settings->toJson());
    }

    public function update(Request $request)
    {
        DB::beginTransaction();

        foreach ($request->json()->all() as $name => $value) {
            Setting::where('name', '=', $name)
                ->update([
                    'value' => $value,
                ]);
        }

        DB::commit();
        //$settings = new Settings(Setting::all());
        return response()->success(__('responses.success'));
    }
}
