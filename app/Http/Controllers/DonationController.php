<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Mail\NewDonation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mollie\Laravel\Facades\Mollie;

class DonationController extends Controller
{
    /**
     * One-off member donation request.
     * Sure, I'll take your money!
     *
     * @return response
     */
    public function createOneTimeMemberDonation(Request $request)
    {
        $amount = $request->amount;
        $user = $request->user();

        // Create payment and send it to the frontend
        $payment_href = $this->createDonationPayment($amount, $user->mollie_customer_id, $user->id);

        if (! $payment_href) {
            return response()->statusError(422, __('responses.finance.payment.amount_too_small'));
        }

        return response()->success(__('responses.finance.payment.ready'), [
            'href' => $payment_href,
        ]);
    }

    public function callbackDonationPayment(Request $request)
    {
        $customer_id = $request->get('customerId');
        $payments = Mollie::api()->customers->get($customer_id)->payments();

        foreach ($payments as $payment) {
            // Only process the most recent donation payment, which is the
            // payment on 'top' of the payments array.
            if (! str_contains($payment->description, 'Eenmalige Donatie')) {
                continue;
            }
            if ($payment->status !== 'paid') {
                return response()->statusError(402, __('responses.finance.payment.not_paid'));
            }
            return response()->success(__('responses.finance.payment.thanks'));
        }
        return response()->statusError(402, 'Geen recente donaties gevonden, probeer het opnieuw.');
    }

    public function webhookDonationPayment(Request $request)
    {
        // https://docs.mollie.com/overview/webhooks
        $payment = Mollie::api()->payments()->get($request->input('id'));
        $user = User::where('mollie_customer_id', $payment->customerId)->first();
        if ($payment->status === 'paid' && str_contains($payment->description, 'Eenmalige Donatie')) {
            Mail::to($user->email)->queue(new NewDonation($user, $payment));
        }
    }

    private function createDonationPayment($amount, $customer_id, $user_id = false, $currency = 'EUR')
    {
        $baseUrl = config('app.url');
        $redirectUrl = $baseUrl . '/donate/callback?customerId=' . $customer_id . '&userId=' . $user_id;
        $amount_formatted = number_format((float) $amount, 2);

        if ($amount_formatted < 1.00) {
            return false;
        }

        $payment_data = [
            'amount' => [
                'currency' => $currency,
                'value' => $amount_formatted,
            ],
            'sequenceType' => 'oneoff',
            'description' => 'Eenmalige Donatie aan Stichting The Space Leiden',
            'redirectUrl' => $redirectUrl,
        ];

        if ($customer_id !== false) {
            $payment_data['customerId'] = $customer_id;
        }

        // only use a webhook if we're not in development mode, since Mollie
        // won't be able to reach our localhost and we're not using ngrok-like proxies.
        if (! empty(config('app.mollie_webhook'))) {
            $payment_data['webhookUrl'] = $baseUrl . '/api/payments/donate/webhook';
        }

        $payment = Mollie::api()->payments()->create($payment_data);
        return $payment->_links->checkout->href;
    }
}
