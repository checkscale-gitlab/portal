<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;

class SignupRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required'],
            'prefix' => ['nullable'],
            'last_name' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', Password::min(16)->uncompromised()],
            'phone_number' => ['required', 'phone:AUTO,NL'],
            'postal_code' => ['nullable', 'integer', 'min:1000', 'max:9999'],
            'birthdate' => ['required', 'date', 'after:01-01-1900'],
            'opt_in_news' => ['required', 'boolean'],
            'agree_terms' => ['required', 'accepted'],
            'discord_name' => ['nullable', 'min:5'],

            // TODO: implement hCAPTCHA
            // https://github.com/Scyllaly/hcaptcha
        ];
    }

    // TEMP: gives us access to the validator error (but only the first)
    protected function failedValidation(Validator $validator)
    {
        $message = $validator->errors()->all();
        throw new ValidationException($message[0]);
    }
}
