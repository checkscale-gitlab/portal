<?php

declare(strict_types=1);

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewDonation extends Mailable
{
    use Queueable;

    use SerializesModels;

    public $user;

    public $payment;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, $payment)
    {
        $this->user = $user;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        $payment = $this->payment;
        return $this->subject('Bedankt voor je donatie!')
            ->markdown('emails.newdonation')
            ->with([
                'name' => $user->full_name,
                'donation_amount' => $payment->amount->value,
                'donation_currency' => $payment->amount->currency,
            ]);
    }
}
