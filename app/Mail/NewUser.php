<?php

declare(strict_types=1);

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewUser extends Mailable
{
    use Queueable;

    use SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        return $this->subject('Welkom bij de Space Leiden!')
            ->markdown('emails.newuser')
            ->with([
                'name' => $user->full_name,
                'next_subscription_payment' => $user->subscription->nextPaymentDate,
                'subscription_cost' => $user->subscription->amount,
            ]);
    }
}
