<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddAdminRegistrationPermissions extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $admin_role = Role::findOrCreate('admin');

        $permission = Permission::updateOrCreate([
            'name' => 'add_registrations',
            'guard_name' => 'web',
        ]);

        $permission->assignRole($admin_role);
    }
}
