<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->dateTime('activity_start');
            $table->string('title');
            $table->text('description');
            $table->string('type');  // is this a talk, a workshop, a seminar, or something else?
            $table->string('location');  // a room at Technolab, a virtual place like twitch or discord, etc.
            $table->string('speaker');  // name to use in promo, or optionally a user id within our database? What if there's multiple speakers?
            $table->integer('max_participants')->default(1000);  // usually infinite, sometimes limited
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
