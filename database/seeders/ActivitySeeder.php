<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('activities')->insert([
            'title' => 'Self watering plant with Offerzen',
            'activity_start' => date('Y-m-d H:i:s', strtotime('last thursday -14 day 19:30:00')),
            'speaker' => 'Offerzen',
            'description' => 'Meld je aan via de externe link',
            'type' => 'Workshop',
            'location' => 'Technolab of thuis',
        ]);
        DB::table('activities')->insert([
            'title' => 'Laravel 101',
            'activity_start' => date('Y-m-d H:i:s', strtotime('last thursday -7 day 19:30:00')),
            'speaker' => 'Jeffrey Sloof',
            'description' => 'Altijd al eens willen weten hoe je een goede backend opzet?',
            'type' => 'Workshop',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => 'Flash Talks',
            'activity_start' => date('Y-m-d H:i:s', strtotime('last thursday 19:30:00')),
            'speaker' => 'Verschillende',
            'description' => 'Kom eens kijken wat we zoal doen bij The Space, door middel van drie korte talks.',
            'type' => 'Talk',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => 'Hack The Box',
            'activity_start' => date('Y-m-d H:i:s', strtotime('next thursday 19:30:00')),
            'speaker' => 'Niels van Gijzen',
            'description' => 'Kom kijken hoe Niels een box kraakt',
            'type' => 'Demo',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => "The Hitchhiker's guide to Kubernetes",
            'activity_start' => date('Y-m-d H:i:s', strtotime('next thursday +7 day 19:30:00')),
            'speaker' => 'Q42',
            'description' => 'We kijken naar de livestream van het event van Q42, en er is ruimte om aan je eigen project te werken!',
            'type' => 'Talk',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => 'Open project avond',
            'activity_start' => date('Y-m-d H:i:s', strtotime('next thursday +14 day 19:30:00')),
            'speaker' => '',
            'description' => 'Kom gezellig aan je eigen project werken!',
            'type' => 'Projects',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => 'SEO in a nutshell',
            'activity_start' => date('Y-m-d H:i:s', strtotime('next thursday +21 day 19:30:00')),
            'speaker' => 'Maikel Vuister',
            'description' => 'Hoe zet jij SEO optimaal in? Onze gastspreker Maikel komt er alles over vertellen.',
            'type' => 'Talk',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => 'Open project avond',
            'activity_start' => date('Y-m-d H:i:s', strtotime('next thursday +28 day 19:30:00')),
            'speaker' => '',
            'description' => 'Kom gezellig aan je eigen project werken!',
            'type' => 'Projects',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => 'FPV drone bouwen',
            'activity_start' => date('Y-m-d H:i:s', strtotime('next thursday +35 day 19:30:00')),
            'speaker' => 'Mathijs Bernson',
            'description' => 'Drone bouwer Mathijs komt ons vertellen hoe je een FPV drone maakt.',
            'type' => 'Talk',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => 'Open project avond',
            'activity_start' => date('Y-m-d H:i:s', strtotime('next thursday +42 day 19:30:00')),
            'speaker' => '',
            'description' => 'Kom gezellig aan je eigen project werken!',
            'type' => 'Projects',
            'location' => 'Technolab',
        ]);
        DB::table('activities')->insert([
            'title' => 'Kubernetes introductie',
            'activity_start' => date('Y-m-d H:i:s', strtotime('next thursday +49 day 19:30:00')),
            'speaker' => 'Jacek Smit',
            'description' => 'Jacek komt ons vertellen over hoe Kubernetes werkt, en wat je er mee kan. Wil jij wel eens leren hoe je een cluster opzet, wat een pod is, en hoe je daar je containers in draait? Wees er bij!',
            'type' => 'Workshop',
            'location' => 'Technolab',
        ]);
    }
}
