<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $users = User::all();

        DB::table('news')->insert([
            'user_id' => $users[8]['id'],
            'title' => 'Onze portal is live!',
            'content' => 'Vanaf nu kun je hier alles vinden over komende activiteiten, nieuws, en je lidmaatschapsinformatie',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
