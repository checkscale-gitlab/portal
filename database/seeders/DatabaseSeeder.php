<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            UserSeeder::class,
            ActivitySeeder::class,
            NewsSeeder::class,
            SocialmediaSeeder::class,
            SettingsSeeder::class,
        ]);
    }
}
