<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{not_api}', function () {
    return view('index');
})->where('not_api', '^(?!api).*$'); // I absolutely hate RegEx but it matches anything that's doesn't start with /api
